# Webix Jet Member

Website yang dibuat untuk mengatur keanggotaan dari sebuah organisasi webix jet.

## Teknologi

Teknologi yang digunakan adalah HTML5, CSS3, Javascript, Framework Javascript yaitu Webix Jet.

## Hasil Kerja pada Website
Website ini dapat melakukan pendaftaran anggota dan pengecekan apakah anggota sudah terdaftar. Kekurangannya adalah ketika dalam pendaftaran fungsi async await tidak dapat dijalankan dimana membuat fungsi daftar menjadi agak error tetapi tetap bisa dilakukan. Fungsi untuk melihat anggota hanya bisa mengambil data, yaitu melakukan fetching api dan memproses data dalam tabel. Saya ingin membuat data bisa diedit, kurangnya resource pembelajaran menjadi hal yang agak sulit untuk membuatnya. Tetapi saya tetap mencoba dan berusaha agar bisa terlihat diedit.

## Special Thanks
Terima kasih kepada Pak Heri Samuel Kurniawan yang telah memberi test ini, saya mempelajari hal baru yaitu framework webix. Saya berharap sudah menghasilkan yang terbaik. Dasarnya juga saya bingung ingin membuat web apa, karena tutorial yang cukup sedikit dan bahkan teman teman saya tidak pernah tau webix. Saya mencoba sedikit bereksperimen dengan fungsi dasar javascript dan akhirnya menghasilkan sesuatu yang membuat saya bangga.