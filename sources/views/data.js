import { JetView } from "webix-jet";

export default class DataView extends JetView {
  config() {
    return webix
      .ajax()
      .get("https://glints-redu.herokuapp.com/api/users")
      .then(data => {
        let allData = data.json();
        console.log(data.json());
        return {
          rows: [
            {
              view: "datatable",
              id: "tabelAnggota",
              data: allData.data,
              columns: [
                {
                  id: "_id",
                  header: [
                    { text: "Id" },
                    { text: "<span class='webix-icon fa fa-filter'></span>" }
                  ],
                  fillspace: true,
                  sort: "int"
                },
                {
                  id: "name",
                  header: [{ text: "Nama Anggota" }, { content: "textFilter" }],
                  fillspace: true,
                  sort: "string",
                  editor: "text"
                },
                {
                  id: "email",
                  header: [{ text: `Email` }],
                  fillspace: true
                },
                {
                  id: "isVerified",
                  header: [{ text: "Status Keanggotaan" }],
                  width: 160,
                  editable: true,
                  editor: "text"
                },
                {
                  header: [
                    {
                      text: "Aksi"
                    }
                  ],
                  template: "{common.trashIcon}",
                  width: 100
                }
              ],
              borderless: true,
              yCount: 5,
              scroll: false,
              select: true,
              editable: true,
              editaction: "dblclick",
              type: {
                trashIcon: "<span class='webix-icon fa fa-trash-alt'></span>",
                editIcon: {
                  view: "button",
                  type: "iconButton",
                  icon: "fa fa-trash-alt",
                  click: () => {
                    var sel = $$("clientTable").getSelectedId();
                    if (sel) {
                      var option = confirm("Hapus data ?");
                      if (option) {
                        $$("clientTable").remove(sel.row);
                      }
                    } else return alert("Pilih data terlebih dahulu.");
                  }
                }
              },
              pager: "pager"
            },
            {
              view: "pager",
              id: "pager",
              css: "pager",
              size: 5,
              template: "{common.prev()} {common.pages()} {common.next()}"
            }
          ]
        };
      });
  }
}
