export default {
  rows: [
    {
      view: "label",
      label: "Daftar Keanggotaan"
    },
    {
      view: "form",
      id: "formLogin",
      elements: [
        {
          view: "text",
          label: "Name",
          id: "name",
          placeholder: "John Doe",
          required: true
        },
        {
          view: "text",
          label: "Email",
          id: "email",
          placeholder: "email@example.com",
          required: true
        },
        {
          view: "text",
          label: "Password",
          id: "password",
          type: "password",
          required: true
        },
        {
          cols: [
            {
              view: "button",
              value: "Daftar",
              click: function() {
                {
                  let ambil = {};
                  ambil.name = $$("name").getValue();
                  ambil.email = $$("email").getValue();
                  ambil.password = $$("password").getValue();
                  console.log(ambil);
                  const url = `https://glints-redu.herokuapp.com/api/users`;
                  {
                    try {
                      const response = fetch(url, {
                        method: "POST",
                        body: JSON.stringify(ambil),
                        headers: {
                          "Content-Type": "application/json"
                        }
                      });
                      alert("Pendaftaran sukses, silahkan cek di Keanggotaan");
                      const json = response.json();
                      console.log("Success", JSON.stringify(json));
                      window.reload("false");
                    } catch (error) {
                      console.log("Error", error);
                    }
                  }
                }
              }
            }
          ]
        }
      ]
    }
  ]
};
